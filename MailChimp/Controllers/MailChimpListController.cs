﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MailChimp.Net;
using System.Threading.Tasks;
using System.Net;
using MailChimp.Net.Core;
using MailChimp.Net.Models;


namespace MailChimp.Controllers
{
    public class MailChimpListController : Controller
    {
        private static readonly MailChimpManager Manager = new MailChimpManager("fa38f1088d985cd96e6d49fdce27ed04-us19");
        // GET: MailChimpList
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public async Task<ActionResult> Index()
        {
            var options = new ListRequest
            {
                Limit = 10
            };
            try
            {
                var model = await Manager.Lists.GetAllAsync(options);
                return View(model);
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }


        public async Task<ActionResult> Detail(string id)
        {
            try
            {
                var model = await Manager.Lists.GetAsync(id);
                return View(model);
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }

        public async Task<ActionResult> Create()
        {
            var list = new List
            {
                Name = "API Created List",
                Contact = new Contact
                {
                    Company = "Foo Inc",
                    Address1 = "123 Main St.",
                    City = "Anytown",
                    State = "ME",
                    Zip = "04000",
                    Country = "US"
                },
                PermissionReminder = "You were added to the list as part of an automated process that",
                CampaignDefaults = new CampaignDefaults
                {
                    FromEmail = "jayanth90s@gmail.com",
                    FromName = "Doug Vanderweide",
                    Subject = "Email message from dynamically created List",
                    Language = "en-us"
                },
                EmailTypeOption = true
            };

            try
            {
                var model = await Manager.Lists.AddOrUpdateAsync(list);
                return RedirectToAction("Detail", new { id = model.Id });
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }


        public async Task<ActionResult> Update()
        {
            var list = new List
            {
                Id = "eaad49bd27",
                Name = "API Updated List - Edited",
                Contact = new Contact
                {
                    Company = "FooBar Inc",
                    Address1 = "123 Main St.",
                    City = "Anytown",
                    State = "ME",
                    Zip = "04000",
                    Country = "US"
                },
                PermissionReminder = "You were added to the list as part of an automated process that",
                CampaignDefaults = new CampaignDefaults
                {
                    FromEmail = "jayanth90hk@gmail.com",
                    FromName = "jay Sri",
                    //Subject = "Email message from dynamically created List",
                    //Language = "en-us"
                },
                EmailTypeOption = true
            };

            try
            {
                var model = await Manager.Lists.AddOrUpdateAsync(list);
                return RedirectToAction("Detail", new { id = model.Id });
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }

        public async Task<ActionResult> Delete(string id)
        {
            try
            {
                await Manager.Lists.DeleteAsync(id);
                return RedirectToAction("Index");
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }



    }
}