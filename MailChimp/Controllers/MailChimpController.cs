﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MailChimp.Net;
using MailChimp.Net.Models;
using System.Threading.Tasks;
using MailChimp.Net.Core;
using System.Net;

namespace MailChimp.Controllers
{
    public class MailChimpController : Controller
    {
        private static MailChimpManager Manager = new MailChimpManager("fa38f1088d985cd96e6d49fdce27ed04-us19");
        // GET: MailChimp
        public ActionResult Index()
        {
            return View();
        }
            
        public async Task<ActionResult> SentCampaigns()
        {
            var options = new CampaignRequest
            {
                ListId = "äbc123",
                Status = CampaignStatus.Sent,
                SortOrder = CampaignSortOrder.DESC,
                Limit = 10
            };

            try
            {
                var model = await Manager.Campaigns.GetAllAsync(options);
                return View(model);
            }

            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }

            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }

        }

    }
}