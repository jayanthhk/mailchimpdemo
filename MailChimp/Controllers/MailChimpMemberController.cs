﻿using System;
using System.Web.Mvc;
using MailChimp.Net;
using MailChimp.Net.Models;
using MailChimp.Net.Core;
using System.Threading.Tasks;
using System.Net;
using System.Collections.Generic;



namespace MailChimp.Controllers
{


    public class MailChimpMemberController : Controller
    {

        private static readonly MailChimpManager Manager = new MailChimpManager("fa38f1088d985cd96e6d49fdce27ed04-us19");
        // GET: MailChimpMember
        //public ActionResult Index()
        //{
        //    return View();
        //}
    

        public async Task<ActionResult> Index()
        {
            try
            {
                ViewBag.ListId = "99b084ced8";
                var model = await Manager.Members.GetAllAsync("99b084ced8",
                    new MemberRequest { Limit = 100, Status = Status.Subscribed });
                return View(model);
            }

            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }


        public async Task<ActionResult> Detail(string id = "jayanth.pragso@gmail.com")
        {
            try
            {
                var model = await Manager.Members.GetAsync("99b084ced8", id);
                return View(model);
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }

 
        public async Task<ActionResult> Create()
        {
           

            var member = new Member
            {
                EmailAddress = "jayanth.pragso@gmail.com",
                StatusIfNew = Status.Subscribed,
                EmailType = "html",
                IpSignup = Request.UserHostAddress,
                TimestampSignup = DateTime.UtcNow.ToString("s"),
                //MergeFields = new Dictionary<string, string>
                //{
                //    { "FNAME", "Foo" },
                //    { "LNAME", "Bar" }
                //}
                MergeFields = new Dictionary<string, object>
                {
                    { "FNAME", "Foao" },
                    { "LNAME", "Bara" }
                }

            };

            try
            {
                var result = await Manager.Members.AddOrUpdateAsync("99b084ced8", member);
                return RedirectToAction("Detail", new { id = result.EmailAddress });
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }

        public async Task<ActionResult> Delete(string id = "jayanth.pragso@gmail.com")
        {
            try
            {
                await Manager.Members.DeleteAsync("99b084ced8", id);
                return RedirectToAction("Index");
            }
            catch (MailChimpException mce)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway, mce.Message);
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.ServiceUnavailable, ex.Message);
            }
        }
    }
}